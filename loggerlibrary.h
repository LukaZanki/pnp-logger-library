// LOGGER LIBRARY
#ifndef LOGGERLIBRARY_H
#define LOGGERLIBRARY_H

#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <cstdio>
#include <ctime>
#include <mutex> 

inline std::string NowTime();

enum TLogLevel {logERROR, logWARNING, logINFO, logDEBUG, logDEBUG1,logDEBUG2,logOFF};
//MULTITHREADING
std::mutex mtx;

template <typename T>
class Log
{
	public:
	   Log();
	   virtual ~Log();
	   std::ostringstream& Get(TLogLevel level = logINFO);
	   static TLogLevel& ReportingLevel();
	   static std::string ToString(TLogLevel level);
	protected:
	   std::ostringstream os;
	private:
	   Log(const Log&);
	   Log& operator =(const Log&);
	   TLogLevel messageLevel;
};

//MAKE THE LOG MESSAGE
template <typename T>
inline std::ostringstream& Log<T>::Get(TLogLevel level)
{
	os << "[" << NowTime();
	os << "][" << ToString(level) << "] ";
	//os << std::string(level > logDEBUG ? level - logDEBUG : 0, '\t'); //add indent to deeper levels
	messageLevel = level;
	
	return os;
}

//CONSTRUCTOR
template <typename T>
inline Log<T>::Log()
{
}

//DESTRUCTOR
template <typename T>
inline Log<T>::~Log()
{
   if (messageLevel >= Log::ReportingLevel() && Log::ReportingLevel()!=logOFF)
   {
      os << std::endl;
      T::Output(os.str());
   }
}

//CURRENT/SET REPORTING LEVEL
template <typename T>
inline TLogLevel& Log<T>::ReportingLevel()
{
    static TLogLevel reportingLevel = logDEBUG1;
    return reportingLevel;
}

//RETURN THE LEVEL NAME
template <typename T>
inline std::string Log<T>::ToString(TLogLevel level)
{
	static const char* const buffer[] = {"ERROR", "WARNING", "INFO", "DEBUG", "DEBUG1","DEBUG2","LOGOFF"};
    return buffer[level];
}

//CLASS FILE HANDLER
class Output2FILE
{
	public:
	    static void Output(const std::string& msg);
	    static FILE*& StreamInit();
	    static void SetStream(FILE* pFile);
};

//DEFAULT FILE TO LOG
inline FILE*& Output2FILE::StreamInit()
{
    static FILE* pFile = fopen("filetolog.log", "a"); //default file
    return pFile;
}

//CHANGE LOG FILE
inline void Output2FILE::SetStream(FILE* pFile)
{
    std::lock_guard<std::mutex> lock (mtx);
    StreamInit() = pFile;
}

//WRITE
inline void Output2FILE::Output(const std::string& msg)
{
    std::lock_guard<std::mutex> lock (mtx);
    FILE* pathStream = StreamInit();
    if (!pathStream)
        return;
    fprintf(pathStream, "%s", msg.c_str());
    fflush(pathStream);
}

typedef Log<Output2FILE> FILELog;

#define FILELOG_MAX_LEVEL 6 //logDEBUG2 highest level of writting with logoff as 7
#define FILE_LOG(level) \
    if (level > FILELOG_MAX_LEVEL) ;\
    else if ((level > FILELog::ReportingLevel() && FILELog::ReportingLevel()!=logOFF) || !Output2FILE::StreamInit() ); \
    else FILELog().Get(level)

//GET THE CURRENT TIME
inline std::string NowTime()
{
	char buffer[256];
    time_t rawtime;
	struct tm * timeinfo;
	time (&rawtime);
	timeinfo = localtime(&rawtime);
    strftime(buffer, sizeof(buffer), "%a %b %d %H:%M:%S %Y", timeinfo);
    std::string str(buffer);
    return buffer;
}

#endif
