#include "loggerlibrary.h"

#include <iostream>

int main(int argc, char* argv[])
{
	FILELog::ReportingLevel() = logDEBUG;
	const int count = 5;
	int var = 2;
	FILE_LOG(logDEBUG) << "A loop where i<" << count << " do: var = 2; var++;";
	FILELog::ReportingLevel() = logDEBUG1;
	for (int i = 0; i != count; ++i)
	{
		var++;
		FILE_LOG(logDEBUG1) << "var = " << var <<" where i = " << i;
	}
	//FILELog::ReportingLevel() = logOFF;
	FILE_LOG(logDEBUG) << "Loop end";
}

